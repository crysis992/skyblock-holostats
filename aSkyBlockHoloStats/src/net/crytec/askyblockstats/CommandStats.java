package net.crytec.askyblockstats;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.filoghost.holographicdisplays.api.Hologram;

public class CommandStats implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if (!(sender instanceof Player)) {
			sender.sendMessage("This Command can only be executed as a Player!");
			return true;
		}

		Player p = (Player) sender;

		if (!p.hasPermission("askyblock.admin.holoboard")) {
			p.sendMessage("�cYou lack the proper permission to use this command.");
			return true;
		}

		if (args.length == 1) {
			if (args[0].equalsIgnoreCase("list")) {

				StringBuilder sb = new StringBuilder();

				for (String s : ASkyBlockStats.getInstance().boards.keySet()) {
					sb.append(s + ", ");
				}

				String list = sb.toString().trim();
				list = StringUtils.chomp(list);
				p.sendMessage("�7List of all Top10 Holo boards:");
				p.sendMessage("�7" + list);
				return true;
			}

			else if (args[0].equalsIgnoreCase("reload")) {
				ASkyBlockStats.getInstance().removeAll();
				ASkyBlockStats.getInstance().reloadConfig();
				ASkyBlockStats.getInstance().loadBoards();
				p.sendMessage("�aAll Boards have been sucessfully reloaded.");
				return true;
			} else {
				sendHelp(p);
				return true;
			}
		}

		else if (args.length == 2) {

			if (args[0].equalsIgnoreCase("create")) {
				Hologram holo = ASkyBlockStats.getInstance().createHologram(args[1], p.getLocation());

				ASkyBlockStats.getInstance().getConfig().set("boards." + args[1], Util.locToString(holo.getLocation()));
				ASkyBlockStats.getInstance().saveConfig();
				p.sendMessage("�aHologram added.");
				return true;
			}

			else if (args[0].equalsIgnoreCase("delete")) {

				if (ASkyBlockStats.getInstance().deleteHolo(args[1])) {
					p.sendMessage("�cHologram deleted.");
				} else {
					p.sendMessage("�cCould not delete hologram - hologram not found!");
				}
				return true;
			} else {
				sendHelp(p);
				return true;
			}

		} else {
			sendHelp(p);
			return true;
		}
	}

	private void sendHelp(Player p) {
		p.sendMessage("�6== SkyBlock Holo Stats ==");
		p.sendMessage("�e/asstats list �b - List all boards with their name.");
		p.sendMessage("�e/asstats reload �b - Reload all boards and configuration file.");
		p.sendMessage("�e/asstats create <Name> �b - Creates a new top 10 board");
		p.sendMessage("�e/asstats delete <Name> �b - Delete the given board");
	}
}
