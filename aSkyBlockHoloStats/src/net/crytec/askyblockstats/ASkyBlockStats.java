package net.crytec.askyblockstats;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

import net.md_5.bungee.api.ChatColor;

public class ASkyBlockStats extends JavaPlugin {

	private static ASkyBlockStats instance;
	private boolean isAcid;
	public HashMap<String, Hologram> boards = new HashMap<String, Hologram>();

	@Override
	public void onLoad() {
		ASkyBlockStats.instance = this;
		updateConfig("topline", "&6SkyBlock Top 10");
		updateConfig("format", "&b#%rank% &6%nickname% &ewith Island Level&6 %level%");
		updateConfig("update interval", 15);
		saveConfig();
	}

	@Override
	public void onEnable() {

		this.checkVersion();

		if (!checkDependency()) {
			Bukkit.getLogger().severe("Cannot find dependency. Make sure ASkyBlock/AcidIsland and HolographicDisplays are installed.");
			Bukkit.getLogger().severe("Plugin cannot start until dependency errors are solved!");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		getCommand("asstats").setExecutor(new CommandStats());
		loadBoards();
		int interval = (20 * 60) * getConfig().getInt("update interval");
		if (interval > 0) {
			Bukkit.getScheduler().runTaskTimer(this, new Runnable() {

				public void run() {
					for (String s : boards.keySet()) {
						createHologram(s, boards.get(s).getLocation());
					}
				}

			}, 60L, interval);
		} else {
			Bukkit.getLogger().info("Interval has been set below 0 - Boards will only update on server start!");
		}
		new Metrics(this);
	}

	@Override
	public void onDisable() {
		removeAll();
	}

	public void removeAll() {
		for (Hologram holo : boards.values()) {
			holo.delete();
		}
	}

	public static ASkyBlockStats getInstance() {
		return ASkyBlockStats.instance;
	}

	public boolean checkDependency() {
		if (Bukkit.getPluginManager().getPlugin("HolographicDisplays") == null) {
			return false;
		}

		if (this.isAcid) {
			if (Bukkit.getPluginManager().getPlugin("AcidIsland") == null) {
				return false;
			}
		} else {
			if (Bukkit.getPluginManager().getPlugin("ASkyBlock") == null) {
				return false;
			}
		}
		return true;
	}

	public Hologram createHologram(String name, Location loc) {
		if (boards.containsKey(name)) {
			boards.get(name).delete();
			boards.remove(name);
		}
		Hologram hg = HologramsAPI.createHologram(this, loc);
		hg.appendTextLine(ChatColor.translateAlternateColorCodes('&', getConfig().getString("topline")));

		Map<UUID, Long> top;

		if (this.isAcid) {
			top = com.wasteofplastic.acidisland.TopTen.getTopTenList();
		} else {
			top = com.wasteofplastic.askyblock.TopTen.getTopTenList();
		}
		int rank = 1;
		for (UUID player : top.keySet()) {
			if (rank > 10) {
				continue;
			}
			String nickname = Bukkit.getOfflinePlayer(player).getName();
			long level = top.get(player);
			String format = getConfig().getString("format").replaceAll("%rank%", "" + rank).replaceAll("%nickname%", nickname).replaceAll("%level%", "" + level);
			format = ChatColor.translateAlternateColorCodes('&', format);
			hg.appendTextLine(format);
			rank++;
		}
		boards.put(name, hg);
		return hg;
	}

	public boolean deleteHolo(String name) {
		if (!boards.containsKey(name)) {
			return false;
		}
		getConfig().set("boards." + name, null);
		saveConfig();
		boards.get(name).delete();
		boards.remove(name);
		return true;
	}

	public void loadBoards() {
		ConfigurationSection sec = getConfig().getConfigurationSection("boards");
		if (sec == null) {
			Bukkit.getLogger().info("No boards have been created yet.");
			return;
		}

		for (String s : sec.getKeys(false)) {
			Location loc = Util.StringToLoc(getConfig().getString("boards." + s));
			if (loc == null) {
				continue;
			}
			if (loc.getWorld() == null) {
				continue;
			}
			createHologram(s, loc);
		}
	}

	public void checkVersion() {
		if (Bukkit.getPluginManager().getPlugin("AcidIsland") != null) {
			getLogger().info("Hooked into AcidIsland!");
			this.isAcid = true;
		} else {
			getLogger().info("Hooked into ASkyBlock!");
			this.isAcid = false;
		}
	}

	private void updateConfig(String path, Object value) {
		if (!getConfig().isSet(path)) {
			getConfig().set(path, value);
		}
	}
}