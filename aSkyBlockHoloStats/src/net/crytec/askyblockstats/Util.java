package net.crytec.askyblockstats;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Util {

	public static String locToString(Location loc) {
		int x = loc.getBlockX();
		int y = loc.getBlockY();
		int z = loc.getBlockZ();
		float pitch = loc.getPitch();
		float yaw = loc.getYaw();
		String world = loc.getWorld().getName();

		String sep = ";";
		String compact = world + sep + x + sep + y + sep + z + sep + pitch + sep + yaw;
		compact = compact.replace('.', '_');
		return compact;
	}

	public static Location StringToLoc(String location) {
		location = location.replace('_', '.');
		String[] l = location.split(";");
		String world = l[0];
		int x = Integer.parseInt(l[1]);
		int y = Integer.parseInt(l[2]);
		int z = Integer.parseInt(l[3]);
		float pitch = Float.parseFloat(l[4]);
		float yaw = Float.parseFloat(l[5]);

		Location loc = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);

		return loc;
	}

}
